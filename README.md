# Python Seven Segment OCR

Seven segment display OCR using simple image processing.
`Pysso` use simplest numpy and PIL operations to get the number from an image. 

The `ocr()` function count the pixels of each segment and if more than 50% are black, then than segment is on.

Before, you should apply the `enhaced_img()` function to improve the contrast of the image. With this function you'll be applying the unsharp mask filter, convert to gray scale and then to black and white image.

We recommend the following steps:

1. Crop the entire display from the images and store in a directory
2. Read the images from the previous directory and:
  - crop each digit (you need the coordinates of each digit)
  - set the coordinates of each segment (in a digit image)
  - apply the filter to enhace the image
  - ocr from each digit

## 1- Crop the display

```python
import pysso

mydir = "/home/emi/images"
xini, yini = 100, 230
xend, yend = 240, 490
pysso.crop_display(bbox=(xini,yini,xend,yend), dst="outdir", ext="jpeg", src=):

```

## 2- Set info to OCR
- Takes the coordinates of each digit, for example if the picture has 4 digits (for example 312.4) you need the xini,yini and xend, yend of each digit.
- Then you need the coordinates of each segment of a digit

**It is important** the order of the bounding box segments list. Seven segments has been enumerated following this:

<img src="segments.png" alt="Kitten" title="Segments enumeration" width="150" height="100" />

So, the first element of ``bbox_segments`` list correspond to 0 segmend, the second correspond to the 1 segment and so on.

Each element of the ``bbox_segments`` list is a tuple with the top-left bottom-right corner coordinates, that is: xini,yini, xend, yend

```python
# the croped image
f = "/home/emi/outdir/mypic.jpeg"

# boundig box list for each digit coordinates
# 0 -> floating part, 1 -> unit, 2-> tens, 3-> hundred 
bbox_digits = [(102, 2, 125, 44),
    (78, 2, 101, 44),
    (51, 3, 74, 45),
    (25, 3, 48, 45)
]
# coordinates of each segment (see documentation)
bbox_segments = [
    (  2,  5,  8, 19 ),
    (  0, 23,  6, 37 ), 
    (  4, 37, 15, 42 ),
    ( 15, 24, 21, 38 ),
    ( 17,  5, 23, 19 ),
    (  7,  0, 18,  5 ),
    (  6, 19, 17, 24 )
]
# improve the image using a unsharp mask filter, 
img = pysso.enhaced_img(f)

# process each image and get numpy array to make then ocr of each digit
dd = pysso.crop_digit(img, bbox_digit=bbox_digits[0], bbox_segments=bbox_segments)
du = pysso.crop_digit(img, bbox_digit=bbox_digits[1], bbox_segments=bbox_segments)
dt = pysso.crop_digit(img, bbox_digit=bbox_digits[2], bbox_segments=bbox_segments)
dh = pysso.crop_digit(img, bbox_digit=bbox_digits[3], bbox_segments=bbox_segments)

# show the number applying the ocr 
print(pysso.ocr(dd))
print(pysso.ocr(du))
print(pysso.ocr(dt))
print(pysso.ocr(dh))
```

## Requierements

numpy and pillow