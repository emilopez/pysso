import glob
import PIL
import numpy as np
from PIL import Image, ImageFilter

def crop_display(from_pos=0, to_pos=None, tag="crop", bbox=(), dst=""):
    path = "/home/emiliano/data/dev/proyectos_git/pysuelo/data/fotos_balanzas"
    photo = glob.glob(f"{path}/*.jpeg")
    photo.sort()

    for im in photo[from_pos:to_pos]:
        img = Image.open(im)
        img = img.crop(box=bbox)
        #img = img.rotate(-10, PIL.Image.NEAREST, expand = 1)
        fname = im.split("/")[-1]
        img.save(f"{dst}/{fname}_{tag}.jpg")

def crop_digit2(img, bbox, segmentwidth=4, segmentlen=11):
    """return list of PIL images for each segment"""
    segment = list()
    
    digit = Image.open(img).crop(box=bbox)
    # bounding box of each segment for a digit
    bbox_s0 =  0,  4,  4, 15
    bbox_s1 =  0, 18,  4, 29 
    bbox_s2 =  2, 27, 13, 31
    bbox_s3 = 10, 18, 14, 29
    bbox_s4 = 11,  4, 15, 15
    bbox_s5 =  2,  0, 13,  4
    bbox_s6 =  2, 14, 13, 18

    segment.append(digit.crop(box=bbox_s0))
    segment.append(digit.crop(box=bbox_s1))
    segment.append(digit.crop(box=bbox_s2))
    segment.append(digit.crop(box=bbox_s3))
    segment.append(digit.crop(box=bbox_s4))
    segment.append(digit.crop(box=bbox_s5))
    segment.append(digit.crop(box=bbox_s6))

    return segment

def crop_digit3(img, bbox, segmentwidth=4, segmentlen=11):
    """tanda 3> return list of np array for each segment"""
    segment = list()
    
    digit = img.crop(box=bbox)
    # bounding box of each segment for a digit
    bbox_s0 =  2,  5,  8, 19
    bbox_s1 =  0, 23,  6, 37 
    bbox_s2 =  4, 37, 15, 42
    bbox_s3 = 15, 24, 21, 38
    bbox_s4 = 17,  5, 23, 19
    bbox_s5 =  7,  0, 18,  5
    bbox_s6 =  6, 19, 17, 24

    segment.append(np.asarray(digit.crop(box=bbox_s0)))
    segment.append(np.asarray(digit.crop(box=bbox_s1)))
    segment.append(np.asarray(digit.crop(box=bbox_s2)))
    segment.append(np.asarray(digit.crop(box=bbox_s3)))
    segment.append(np.asarray(digit.crop(box=bbox_s4)))
    segment.append(np.asarray(digit.crop(box=bbox_s5)))
    segment.append(np.asarray(digit.crop(box=bbox_s6)))

    return segment

def crop_digit(img, pos='u'):
    """return list of PIL images for each segment"""
    segment = list()
    box_digit = {"u":(52, 0, 67, 30), 
                 "t":(33, 0, 48, 30),
                 "h":(15, 0, 30, 30)}
    
    u = Image.open(img).crop(box=box_digit[pos])
    # bounding box of each segment for a digit
    bbox_s0 = 0, 3, 5, (3+9)
    bbox_s1 = 0, 16, 5, (16+9)
    bbox_s2 = 3, 25, (3+7), (25+5)
    bbox_s3 = 10, 16, (10+5), (16+9)
    bbox_s4 = 10, 3, 10+5, (3+9)
    bbox_s5 = 4, 0, (4+8), 3
    bbox_s6 = 4, 12, (4+8), 12+4

    segment.append(u.crop(box=bbox_s0))
    segment.append(u.crop(box=bbox_s1))
    segment.append(u.crop(box=bbox_s2))
    segment.append(u.crop(box=bbox_s3))
    segment.append(u.crop(box=bbox_s4))
    segment.append(u.crop(box=bbox_s5))
    segment.append(u.crop(box=bbox_s6))

    return segment



def calc_mean(img, white_level=195):
    """Mean of a img
        - input
            img: PIL image of a segment
            white_level: if a pixel exceed this value it is white
        - output
            mean between pixels of the image
    
    """
    white = 255
    black = 0
    # to gray scales
    gray = img.convert('L')
    # to black and white
    bw = gray.point(lambda x: white if x > white_level else black, '1')
    # to numpy nd-array
    return np.asarray(bw).mean()

def ocr(segment_list):
    
    # NOW: near to 1 is black, near to 0 is white (segment on or segment off)
    digit = np.array([1-calc_mean(segment) for segment in segment_list])
    # only binary digits, 1 or 0
    digit[digit >= 0.5] = 1
    digit[digit < 0.5]  = 0
    # nparray to tuples (hashable) to be used as dict key
    digit = tuple(digit)
    
    numbers = {(1, 1, 1, 1, 1, 1, 0): 0,
               (0, 0, 0, 1, 1, 0, 0): 1, 
               (0, 1, 1, 0, 1, 1, 1): 2,
               (0, 0, 1, 1, 1, 1, 1): 3,
               (1, 0, 0, 1, 1, 0, 1): 4,
               (1, 0, 1, 1, 0, 1, 1): 5,
               (1, 1, 1, 1, 0, 1, 1): 6, 
               (0, 0, 0, 1, 1, 1, 0): 7, 
               (1, 1, 1, 1, 1, 1, 1): 8,
               (1, 0, 0, 1, 1, 1, 1): 9
    }
    return numbers.get(digit, "x")
def ocr3(segment_list):
    # NOW: near to 1 is black, near to 0 is white (segment on or segment off)
    digit = np.array([(s==0).sum()/s.size >0.5 for s in segment_list])
    digit = tuple(digit)
    numbers = {(1, 1, 1, 1, 1, 1, 0): 0,
               (0, 0, 0, 1, 1, 0, 0): 1, 
               (0, 1, 1, 0, 1, 1, 1): 2,
               (0, 0, 1, 1, 1, 1, 1): 3,
               (1, 0, 0, 1, 1, 0, 1): 4,
               (1, 0, 1, 1, 0, 1, 1): 5,
               (1, 1, 1, 1, 0, 1, 1): 6, 
               (0, 0, 0, 1, 1, 1, 0): 7, 
               (1, 1, 1, 1, 1, 1, 1): 8,
               (1, 0, 0, 1, 1, 1, 1): 9
    }
    return numbers.get(digit, "x")
    

def ocrB2_tanda2():
    """Tanda de fotos desde 2020-03-19 20:15 al final"""
    path = "display_img/"
    display_photos = glob.glob(f"{path}/*.jpg")
    display_photos.sort()

    fout = open("balanza2.csv", "w")

    for f in display_photos:
        uni_digit = crop_digit(f, pos="u")
        ten_digit = crop_digit(f, pos="t")
        hun_digit = crop_digit(f, pos="h")

        number = f"3{ocr(hun_digit)}{ocr(ten_digit)}{ocr(uni_digit)}"
        fname = f.split("/")[-1]
        datetime = fname[0:10] + " " +fname[11:13] + ":" + fname[14:16]
        #print(datetime, number)
        fout.write(datetime)
        fout.write(";")
        fout.write(number)
        fout.write("\n")

    fout.close()

def ocrB2_tanda1():
    """Recortes del display de B2
    Tanda 1: A partir de 2020-02-26 00:15, pos 1049
               al 2020-03-16 11:15, pos 2825
    """
    path = "display_img/"
    display_photos = glob.glob(f"{path}/tanda1/*.jpg")
    display_photos.sort()

    fout = open("balanza2_tanda1.csv", "w")
    # bbox for each digit
    bbox_u = (56, 2, 71, 33)
    bbox_t = (37, 2, 52, 33)
    bbox_h = (18, 1, 33, 32)

    for f in display_photos:
        uni_digit = crop_digit2(f, bbox=bbox_u)
        ten_digit = crop_digit2(f, bbox=bbox_t)
        hun_digit = crop_digit2(f, bbox=bbox_h)

        number = f"3{ocr(hun_digit)}{ocr(ten_digit)}{ocr(uni_digit)}"
        fname = f.split("/")[-1]
        datetime = fname[0:10] + " " +fname[11:13] + ":" + fname[14:16]
        print(datetime, number)
        fout.write(datetime)
        fout.write(";")
        fout.write(number)
        fout.write("\n")

    fout.close()

def ocrB1_tanda3():
    """Recortes del display de B2
        Tanda 3: desde 2020-04-16 13:00 (pos 5769) al 
              2020-06-12 17:15 (final)
    """
    path = "display_img/"
    display_photos = glob.glob(f"{path}/tanda3/*.jpg")
    display_photos.sort()

    fout = open("balanza2_tanda3.csv", "w")
    # bbox for each digit
    bbox_d = (102, 2, 125, 44)
    bbox_u = (78, 2, 101, 44)
    bbox_t = (51, 3, 74, 45)
    bbox_h = (25, 3, 48, 45)

    for f in display_photos:
        img = enhaced_img(f)
        segments_d = crop_digit3(img, bbox=bbox_d)
        segments_u = crop_digit3(img, bbox=bbox_u)
        segments_t = crop_digit3(img, bbox=bbox_t)
        segments_h = crop_digit3(img, bbox=bbox_h)

        number = f"3{ocr3(segments_h)}{ocr3(segments_t)}{ocr3(segments_u)}.{ocr3(segments_d)}"
        fname = f.split("/")[-1]
        datetime = fname[0:10] + " " +fname[11:13] + ":" + fname[14:16]
        print(datetime, number)
        fout.write(datetime)
        fout.write(";")
        fout.write(number)
        fout.write("\n")

    fout.close()


        



        

def tanda2():
    """Recortes del display de B2
        Tanda 2: display B2, del 2020-03-19_20_15.jpeg, posicion 3157 al final
    """
    bbox_B2 = 866, 666, 936, 699
    crop_display(from_pos=3157, tag="displayB2", bbox=bbox_B2)

def tanda1():
    """Recortes del display de B2
    Tanda 1: A partir de 2020-02-26 00:15, pos 1049
               al 2020-03-16 11:15, pos 2825
    """
    bbox_B2 = 867, 744, 939, 778
    crop_display(from_pos=1049, to_pos=2825, tag="displayB2", bbox=bbox_B2, dst="display_img/tanda1")

def tanda3():
    """Recortes del display B1
        Desde 2020-04-16 13:00 (pos 5769) al 
              2020-06-12 17:15 (final)
    """
    bbox_B1 = 337, 676, 462, 722
    crop_display(from_pos=5769, tag="displayB1", bbox=bbox_B1, dst="display_img/tanda3")

def unsharpmask_filter(img):
    """https://pillow.readthedocs.io/en/3.1.x/reference/ImageFilter.html#"""
    return img.filter(ImageFilter.UnsharpMask(radius=26.197, percent=70, threshold=0))

def enhaced_img(f):
    """mejorar imagen para OCR
        - aplicar filtro
        - pasa a escala de grises
        - convierte a blanco y negro y retorna
    """
    img_orig = Image.open(f)
    img_enhaced = unsharpmask_filter(img_orig)
    gray = img_enhaced.convert('L')
    bw = gray.point(lambda x: 255 if x > 127 else 0, '1')
    return bw

if __name__ == "__main__":
    #tanda2()
    #tanda1()
    #tanda3()
    #ocrB2_tanda1()
    ocrB1_tanda3()
    #pass
    
    
