import glob
import PIL
import numpy as np
from PIL import Image, ImageFilter

def crop_display(from_pos=0, to_pos=None, tag="crop", bbox=(), dst=None,src=None, ext="jpeg"):
    """crop the bounding box section"""

    photo = glob.glob(f"{src}/*.{ext}")
    photo.sort()

    for im in photo[from_pos:to_pos]:
        img = Image.open(im)
        img = img.crop(box=bbox)
        #img = img.rotate(-10, PIL.Image.NEAREST, expand = 1)
        fname = im.split("/")[-1]
        img.save(f"{dst}/{fname}_{tag}.{ext}")

def crop_digit(img, bbox_digit, bbox_segments):
    """tanda 3> return list of np array for each segment"""
     
    digit = img.crop(box=bbox_digit)
    segment = [np.asarray(digit.crop(box=segment)) for segment in bbox_segments]
    return segment


def ocr(segment_list):
    # NOW: near to 1 is black, near to 0 is white (segment on or segment off)
    digit = np.array([((s==0).sum()/s.size) >= 0.5 for s in segment_list])
    digit = tuple(digit)
    numbers = {(1, 1, 1, 1, 1, 1, 0): 0,
               (0, 0, 0, 1, 1, 0, 0): 1, 
               (0, 1, 1, 0, 1, 1, 1): 2,
               (0, 0, 1, 1, 1, 1, 1): 3,
               (1, 0, 0, 1, 1, 0, 1): 4,
               (1, 0, 1, 1, 0, 1, 1): 5,
               (1, 1, 1, 1, 0, 1, 1): 6, 
               (0, 0, 0, 1, 1, 1, 0): 7, 
               (1, 1, 1, 1, 1, 1, 1): 8,
               (1, 0, 0, 1, 1, 1, 1): 9
    }
    return numbers.get(digit, "x")

def unsharpmask_filter(img):
    """https://pillow.readthedocs.io/en/3.1.x/reference/ImageFilter.html#"""
    return img.filter(ImageFilter.UnsharpMask(radius=26.197, percent=70, threshold=0))

def enhaced_img(f):
    """improve the image to OCR
        - apply unsharp mask filter
        - convert to gray scale
        - convert to black and white image
    """
    img_orig = Image.open(f)
    img_enhaced = unsharpmask_filter(img_orig)
    gray = img_enhaced.convert('L')
    bw = gray.point(lambda x: 255 if x > 127 else 0, '1')
    return bw


if __name__ == "__main__":
    print("not exampled implemented yet. Please, read the README file")
    
    
